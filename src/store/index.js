import Vue from 'vue';
import Vuex from 'vuex';
import defaultTasks from './defaultTasks.json';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tasks: []
  },
  getters: {
    getWaitingTasks(state) {
      return state.tasks.filter((task) => {
        return task.status === 'waiting';
      });
    },
    getInProgressTasks(state) {
      return state.tasks.filter((task) => {
        return task.status === 'in progress';
      });
    },
    getDoneTasks(state) {
      return state.tasks.filter((task) => {
        return task.status === 'done';
      });
    }
  },
  mutations: {
    loadTasks: (state) => {
      if(!localStorage.getItem('tasks')) {
        localStorage.setItem('tasks', JSON.stringify(defaultTasks));
      } 
      state.tasks = JSON.parse(localStorage.getItem('tasks'));
    },
    reinitDefaultTasks: (state) => {
      localStorage.setItem('tasks', JSON.stringify(defaultTasks));  
      state.tasks = JSON.parse(localStorage.getItem('tasks'));
    },
    changeTaskStatus: (state, payload) => {
      state.tasks[payload.index].status = payload.status;
      localStorage.setItem('tasks', JSON.stringify(state.tasks));
    },
    removeTask: (state, taskIndex) => {
      let newTasks = [...state.tasks];
      newTasks.splice(taskIndex, 1);
      localStorage.setItem('tasks', JSON.stringify(newTasks));
      state.tasks = newTasks;
    },
    addNewTask: (state, newTask) => {
      let newTasks = [...state.tasks];
      newTasks.push(newTask);
      localStorage.setItem('tasks', JSON.stringify(newTasks));
      state.tasks = newTasks;
    },
    editTask: (state, payload) => {
      let tasks = [...state.tasks];
      tasks[payload.taskIndex] = payload.task;
      localStorage.setItem('tasks', JSON.stringify(tasks));
      state.tasks = tasks;
    }
  },
  actions: {
    loadTasks: (context) => {
      context.commit('loadTasks');
    },
    reinitDefaultTasks: (context) => {
      context.commit('reinitDefaultTasks');
    },
    changeTaskStatus: (context, payload) => {
      context.commit('changeTaskStatus', payload)
    },
    removeTask: (context, taskIndex) => {
      context.commit('removeTask', taskIndex);
    },
    addNewTask: (context, newTask) => {
      context.commit('addNewTask', newTask);
    },
    editTask: (context, payload) => {
      context.commit('editTask', payload);
    }
  },
  modules: {
  }
})
