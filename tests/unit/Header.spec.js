import { shallowMount } from '@vue/test-utils';
import Header from '@/components/Header.vue';

describe('Header', () => {

  it('Header should be in DOM', () => {
    const wrapper = shallowMount(Header, {
    });
    expect(wrapper.find('.header').element).not.toBeUndefined();
  });

  it('Header should has proper title', () => {
    const wrapper = shallowMount(Header, {
    });
    expect(wrapper.find('.header h1').text()).toEqual('Vuex DND Task Manager');
  });

  it('Header should has 2 buttons', () => {
    const wrapper = shallowMount(Header, {
    });
    expect(wrapper.findAll('.header .buttons button').length).toEqual(2);
  });

});