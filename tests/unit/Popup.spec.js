import { shallowMount } from '@vue/test-utils';
import Popup from '@/components/Popup.vue';

describe('Popup', () => {

  let mockData;

  beforeEach(() => {
    mockData =  {
      opened: false
    };
  });

  it('Popup should not be in DOM on mount', () => {
    const wrapper = shallowMount(Popup, {
        data() {
            return mockData;
          }
    });
    expect(wrapper.find('.curtain .popup').element).toBeUndefined();
  });

  it('Popup should be in DOM after call open() function', () => {
    const wrapper = shallowMount(Popup, {
        data() {
            return mockData;
          }
    });
    wrapper.vm.open();
    expect(wrapper.find('.curtain .popup').element).not.toBeUndefined();
  });

  it('Popup should not be in DOM after call close() function', () => {
    const wrapper = shallowMount(Popup, {
        data() {
            return mockData;
          }
    });
    wrapper.vm.open();
    wrapper.vm.close();
    expect(wrapper.find('.curtain .popup').element).toBeUndefined();
  });

  it('Popup should has title element', () => {
    const wrapper = shallowMount(Popup, {
        data() {
            return mockData;
          }
    });
    wrapper.vm.open();
    expect(wrapper.find('.curtain .popup .title').element).not.toBeUndefined();
  });

  it('Popup should has close button', () => {
    const wrapper = shallowMount(Popup, {
        data() {
            return mockData;
          }
    });
    wrapper.vm.open();
    expect(wrapper.find('.curtain .popup .close').element).not.toBeUndefined();
  });

});