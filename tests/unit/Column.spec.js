import { shallowMount } from '@vue/test-utils';
import Column from '@/components/Column.vue';

describe('Column', () => {
  let status, tasks;

  beforeEach(() => {
    status = "in progress";
    tasks = [
        {
            "id": 2,
            "desc": "Add new user",
            "status": "in progress",
            "type": "feature"
        },
        {
            "id": 4,
            "desc": "Update footer",
            "status": "in progress",
            "type": "default"
        },
        {
            "id": 6,
            "desc": "Home link didn't work",
            "status": "in progress",
            "type": "bugfix"
        }
    ];
  });

  it('Column should be in DOM', () => {
    const wrapper = shallowMount(Column, {
        propsData: {
              status, tasks
          }
    });
    expect(wrapper.find('.column').element).not.toBeUndefined();
  });

  it('Column header text should be equal to status', () => {
    const wrapper = shallowMount(Column, {
        propsData: {
              status, tasks
          }
    });
    expect(wrapper.find('.column .label h1').text()).toEqual(status);
  });

  it('Column should display all task from tasks list', () => {
    const wrapper = shallowMount(Column, {
        propsData: {
              status, tasks
          }
    });
    expect(wrapper.findAll('.column .taskList>div').length).toEqual(tasks.length);
  });

});