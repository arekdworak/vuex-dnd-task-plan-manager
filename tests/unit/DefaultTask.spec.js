import { shallowMount } from '@vue/test-utils';
import DefaultTask from '@/components/DefaultTask.vue';

describe('DefaultTask', () => {
  let task;

  beforeEach(() => {
    task = {
      "id": 1,
      "desc": "Apply new CSS styles according to wirefame",
      "status": "waiting",
      "type": "default"
    };
  });

  it('DefaultTask should be in DOM', () => {
    const wrapper = shallowMount(DefaultTask, {
        propsData: {
              task
          }
    });
    expect(wrapper.find('.task').element).not.toBeUndefined();
  });

  it('DefaultTask should be draggable', () => {
    const wrapper = shallowMount(DefaultTask, {
        propsData: {
              task
          }
    });
    expect(wrapper.find('.task').element.draggable).toBeTruthy();
  });

  it('DefaultTask should display type of task', () => {
    const wrapper = shallowMount(DefaultTask, {
        propsData: {
              task
          }
    });
    expect(wrapper.find('.task .header .label').text()).toEqual(task.type);
  });

  it('DefaultTask should be display description of task', () => {
    const wrapper = shallowMount(DefaultTask, {
        propsData: {
              task
          }
    });
    expect(wrapper.find('.task .desc').text()).toEqual(task.desc);
  });

});